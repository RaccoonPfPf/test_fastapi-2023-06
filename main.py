from datetime import date, timedelta
from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from typing import Optional
import jwt
import logging
import hashlib

SECRET_KEY = "16fc305e68d16ca39b4fe38c27b27897"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30
logger = logging.getLogger(__name__)

class Employer:
    def __init__(self, name, salary, promotiondate, password):
        self.name = name
        self.salary = salary
        self.promotiondate = promotiondate
        self.password = password

def generate_mock_data():
    employers = []

    # Placeholder data for employers
    data = [
        ("alice", 50000, "2023-05", hashlib.sha256(b"qwe").hexdigest()),
        ("bob", 60000, "2023-06", hashlib.sha256(b"asd").hexdigest()),
        ("eve", 90000, "2023-07", hashlib.sha256(b"zxc").hexdigest())
    ]
    for d in data:
        employer = Employer(*d)
        employers.append(employer)
    return employers

def find_employer_by_name(name, employers):
    for employer in employers:
        if employer.name == name:
            return employer
    return None

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()

    if expires_delta:
        expire = datetime.utcnow() + expires_delta
        to_encode["exp"] = expire.timestamp()  # Set the 'exp' claim as a timestamp
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def decode_access_token(token):
    try:
        decoded_jwt = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        logger.debug("Decoded JWT: %s", decoded_jwt)
        return decoded_jwt
    except jwt.InvalidTokenError as e:
        logger.error("Invalid token: %s", e)
        raise HTTPException(status_code=401, detail="Invalid token")

app = FastAPI()
security = HTTPBearer()

# Generate mock data for employers
employers = generate_mock_data()

def verify_token(credentials: HTTPAuthorizationCredentials = Depends(security)):
    token = credentials.credentials
    decoded_token = decode_access_token(token)
    return decoded_token

@app.get("/salary/{name}")
async def get_salary(name: str, decoded_token: dict = Depends(verify_token)):
    employer = find_employer_by_name(name, employers)
    if employer:
        return {"name": employer.name, "salary": employer.salary}
    else:
        raise HTTPException(status_code=404, detail="Employer not found")

@app.get("/promotiondate/{name}")
async def get_salary(name: str, decoded_token: dict = Depends(verify_token)):
    employer = find_employer_by_name(name, employers)
    if employer:
        return {"name": employer.name, "promotiondate": employer.promotiondate}
    else:
        raise HTTPException(status_code=404, detail="Employer not found")

@app.post("/login")
async def login(username: str, password: str):
    employer = find_employer_by_name(username, employers)
    if employer:
        # Check password hash
        if hashlib.sha256(password.encode()).hexdigest() == employer.password:
            token = create_access_token({"sub": employer.name})
            return {"token": token}
        else:
            raise HTTPException(status_code=401, detail="Invalid credentials")
    else:
        raise HTTPException(status_code=404, detail="Employer not found")

# Run the FastAPI server
if __name__ == "__main__":
    # logging.basicConfig(level=logging.DEBUG, format="%(asctime)s - %(levelname)s - %(message)s")
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
